﻿public static class GlobalGameEvents
{
    public static string GAMEOVER = "GameOver";
    public static string ASTEROID_DESTROYED = "AsteroidDestroyed";
}