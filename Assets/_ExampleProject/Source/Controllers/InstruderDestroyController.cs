﻿using UnityEngine;
using System.Collections;

public class InstruderDestroyController : MonoBehaviour
{
    void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(collision.gameObject);
    }
}
