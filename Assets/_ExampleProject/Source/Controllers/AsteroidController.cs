﻿using UnityEngine;
using System.Collections;
using Packages.Sacristan.Messenger;

public class AsteroidController : MonoBehaviour
{
    void Awake()
    {
        Destroy(gameObject, 10f); // just in case destroy asteroid after 10 seconds
    }

    [SerializeField]
    private int pointsReceived = 10;

    void OnTriggerEnter2D(Collider2D other)
    {
        switch (other.tag)
        {
            case "Projectile":
                Destroy(other.gameObject);
                OnHitByProjectile(); //handle post hit logic
                Messenger<int>.Broadcast(GlobalGameEvents.ASTEROID_DESTROYED, pointsReceived);
                break;
        }
    }

    void OnHitByProjectile()
    {
        GameManager.SpawnExplosionSFXAtPosition(transform.position);
        Destroy(gameObject);
    }

}
