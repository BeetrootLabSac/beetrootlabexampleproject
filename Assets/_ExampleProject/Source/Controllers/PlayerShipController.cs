﻿using UnityEngine;
using System.Collections;

public class PlayerShipController : MonoBehaviour
{
    [SerializeField]
    private float speed = 10f;

    [SerializeField]
    private float tilt = 1f;

    MapBoundary mapBounds;

    void Start()
    {
        mapBounds = GameManager.MapBoundaries;
    }

    public void HandleMovement()
    {
        float inputHorizontal = Input.GetAxis("Horizontal");
        float inputVertical = Input.GetAxis("Vertical");

        Vector2 velocity = new Vector2(inputHorizontal, inputVertical) * speed;
        Vector2 movementDelta = velocity * Time.deltaTime;

        //NOTE: vector2 -> vector3 (x, y, 0)
        Vector3 finalPos = transform.position + (Vector3)movementDelta;

        Vector3 clampedPosition = new Vector3(
            Mathf.Clamp(finalPos.x, mapBounds.Min.x, mapBounds.Max.x),
            Mathf.Clamp(finalPos.y, mapBounds.Min.y, mapBounds.Max.y),
            0f
        );

        Vector3 eulerRotationVector = Vector3.up * velocity.x * -tilt;
        Quaternion tiltedRotation = Quaternion.Euler(eulerRotationVector);

        transform.position = clampedPosition;
        transform.rotation = tiltedRotation;
    }
}
