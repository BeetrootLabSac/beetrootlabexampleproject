﻿using UnityEngine;
using System.Collections;

public class CombatController : MonoBehaviour
{
    [SerializeField]
    private Transform shootHatch;

    [SerializeField]
    private GameObject projectile;

    [SerializeField]
    private float projectileSpeed;

    private AudioSource audioSource;

    private float destroyProjectileAfterSeconds = 2f;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    public void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        GameObject projectile = Instantiate(this.projectile, shootHatch.position, shootHatch.rotation) as GameObject;

        Rigidbody2D projectileRigidbody = projectile.GetComponent<Rigidbody2D>();

        projectileRigidbody.velocity = Vector3.up * projectileSpeed;

        PlayShotAudio();

        Destroy(projectile, destroyProjectileAfterSeconds);
    }

    private void PlayShotAudio()
    {
        AudioClip[] projectileSFXAudios = GameManager.ProjectileSFXAudios;

        int index = Random.Range(0, projectileSFXAudios.Length);
        AudioClip audioClip = projectileSFXAudios[index];

        audioSource.PlayOneShot(audioClip);
    }

}
