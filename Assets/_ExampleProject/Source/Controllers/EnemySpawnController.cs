﻿using UnityEngine;
using System.Collections;

public class EnemySpawnController : MonoBehaviour
{
    [SerializeField]
    private float asteroidSpeedMin = 1f;
    [SerializeField]
    private float asteroidSpeedMax = 3f;

    [SerializeField]
    private GameObject[] asteroidPrefabs;

    private float repeatRate = 1f;
    private float repeatRateEntropy = 0.05f;
    private float minRepeatRate = 0.01f; 

    void Start()
    {
        InvokeRepeating("SpawnAsteroid", 0f, repeatRate);
        InvokeRepeating("IntensifyAsteroidSpawnRate", 5f, 5f);
    }

    private void IntensifyAsteroidSpawnRate()
    {
        repeatRate -= repeatRateEntropy;
        if (repeatRate <= minRepeatRate)
        {
            CancelInvoke("IntensifyAsteroidSpawnRate");
            return;
        }

        CancelInvoke("SpawnAsteroid");
        InvokeRepeating("SpawnAsteroid", 0f, repeatRate);
    }

    private void SpawnAsteroid()
    {
        MapBoundary bounds = GameManager.MapBoundaries;

        int index = Random.Range(0, asteroidPrefabs.Length);

        Vector3 spawnPos = transform.position;

        spawnPos.x = Random.Range(bounds.Min.x, bounds.Max.x);

        GameObject spawnedMeteor = Instantiate(asteroidPrefabs[index], spawnPos, Quaternion.identity) as GameObject;

        Rigidbody2D meteorRigidbody = spawnedMeteor.GetComponent<Rigidbody2D>();

        float meteorSpeed = Random.Range(asteroidSpeedMin, asteroidSpeedMax);

        int direction = Random.value > 0.5 ? 1 : -1;

        meteorRigidbody.velocity = Vector3.down * meteorSpeed;
        meteorRigidbody.angularVelocity = Random.value * 50 * direction;
    }

}
