﻿using UnityEngine;

public sealed class MapBoundary
{
    private readonly Vector2 min;
    private readonly Vector2 max;

    public MapBoundary(Camera camera, float offset=0f)
    {
        float height = 2f * camera.orthographicSize - offset * 2f;
        float width = height * camera.aspect - offset;

        Vector2 rawWidthHeightVector = new Vector2(width, height);

        this.min = rawWidthHeightVector * -0.5f;
        this.max = rawWidthHeightVector * 0.5f;
    }

    public Vector2 Min
    {
        get { return min; }
    }

    public Vector2 Max
    {
        get { return max; }
    }
}
