﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Packages.Sacristan.Messenger;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager singletone;

    #region Serialized fields
    [SerializeField]
    private Camera orthographicCamera;

    [SerializeField]
    private GameObject explosionSFX;

    [SerializeField]
    private AudioClip gameOverAudio;

    [SerializeField]
    private AudioClip[] projectileSFXAudios;

    [SerializeField]
    private Text scoreText;

    [SerializeField]
    private Text highscoreText;

    [SerializeField]
    private GameObject gameOverPanel;
    #endregion

    private AudioSource audioSource;
    private int score;
    private MapBoundary mapBoundary;

    private int HighScore
    {
        get { return PlayerPrefs.GetInt("HighScore"); }
        set { PlayerPrefs.SetInt("HighScore", score); }
    }

    #region Public Properties
    public static AudioClip[] ProjectileSFXAudios
    {
        get
        {
            return singletone.projectileSFXAudios;
        }
    }

    public static MapBoundary MapBoundaries
    {
        get { return singletone.mapBoundary; }
    }
    #endregion

    public void Retry()
    {
        Scene scene = SceneManager.GetActiveScene();
        SceneManager.LoadScene(scene.name);
    }

    public static void SpawnExplosionSFXAtPosition(Vector3 position)
    {
        GameObject explosionSFX = Instantiate(singletone.explosionSFX, position, Quaternion.identity) as GameObject;
        Destroy(explosionSFX, 2f);
    }

    void Awake()
    {
        if (singletone == null) singletone = this;
        else Destroy(this);

        audioSource = GetComponent<AudioSource>();
        UpdateHighscore();

        mapBoundary = new MapBoundary(orthographicCamera, 0.5f);
    }

    void OnEnable()
    {
        Messenger.AddListener(GlobalGameEvents.GAMEOVER, OnGameOver);
        Messenger<int>.AddListener(GlobalGameEvents.ASTEROID_DESTROYED, OnAsteroidDestroyed);
    }

    void OnDisable()
    {
        Messenger.RemoveListener(GlobalGameEvents.GAMEOVER, OnGameOver);
        Messenger<int>.RemoveListener(GlobalGameEvents.ASTEROID_DESTROYED, OnAsteroidDestroyed);
    }

    private void OnGameOver()
    {
        audioSource.PlayOneShot(gameOverAudio);
        CheckScore();

        StartCoroutine(DisplayGameOverPanel());
    }

    private IEnumerator DisplayGameOverPanel()
    {
        yield return new WaitForSeconds(2f);
        gameOverPanel.SetActive(true);
    }

    private void OnAsteroidDestroyed(int score)
    {
        this.score += score;
        CheckScore();
        UpdateScore();
    }

    private void UpdateScore()
    {
        scoreText.text = "Score: " + score;
    }

    private void UpdateHighscore()
    {
        highscoreText.text = "Highscore: " + HighScore;
    }

    private void CheckScore()
    {
        if (score > HighScore)
        {
            HighScore = score;
            UpdateHighscore();
        }
    }

}
