﻿using UnityEngine;
using System.Collections;

public class PlayerUpdateCycleManager : MonoBehaviour
{
    private PlayerShipController playerShipController;
    private CombatController combatController;

    void Start()
    {
        playerShipController = GetComponent<PlayerShipController>();
        combatController = GetComponent<CombatController>();
    }

    void Update()
    {
        playerShipController.HandleMovement();
        combatController.HandleInput();
    }
}
