﻿using UnityEngine;
using System.Collections;
using Packages.Sacristan.Messenger;

public class PlayerCollisionManager : MonoBehaviour
{

    void OnCollisionEnter2D(Collision2D collision)
    {
        switch (collision.collider.tag)
        {
            case "Asteroid":
                Messenger.Broadcast(GlobalGameEvents.GAMEOVER, MessengerMode.DONT_REQUIRE_LISTENER);
                GameManager.SpawnExplosionSFXAtPosition(transform.position);
                GameManager.SpawnExplosionSFXAtPosition(collision.transform.position);
                Destroy(gameObject);
                Destroy(collision.gameObject);
                break;
        }
    }

}
